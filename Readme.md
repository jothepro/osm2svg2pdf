# OSM 2 SVG 2 PDF
OSM-data rendering tool.

The application uses *XSLT* and *Apache FOP* to convert raw OpenStreetMap data to SVG and to put the
resulting SVG into a PDF.

## Dependencies
- JDK <= 1.8 (higher versions will not work)

## Build

`./mvnw install`

## Run

Run with `java -jar target/osm2svg2pdf-0.1.0.jar`

The webpage is now available under `localhost:8080`

## Functionality

* **changing Zoom level** drag the *Zoom*-bar. (Be careful, zooming out too far takes
    a lot of time to calculate!)
* **changing viewbox** to change the position of what you see, enter the wanted
    coordinates in the lat and lon fields. To ajust the viewbox, drag and drop the
    map.
* **download pdf** when clicking the *Download*-Button the current viewbox is
    put into a pdf. when the calculation is finished, a download-dialog should
    pop up.
* **choose rendering content** in the lower left corner there are checkboxes
    to select which osm data should be rendered. choosing less elements
    improves performance, because the data isn't loaded from osm and therefore
    takes no time on conversion to svg.

## Screenshot

![](screenshot.png)

## Problems
The conversion from osm to svg data is very slow.
