package osm;

/**
 * Created by johannes on 16.06.17.
 */
public class FetchOsmDataException extends Exception {

    public FetchOsmDataException(String message) {
        super(message);
    }
}
