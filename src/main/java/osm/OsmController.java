package osm;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.*;


@RestController
@RequestMapping("/api")
public class OsmController {

    @RequestMapping(method = RequestMethod.POST, value="/svg")
    public String svg(@RequestBody Filter filter) {
        try {
            Osm osm = new Osm(filter);
            return osm.getSvgAsString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value="/pdf", produces="application/pdf")
    public byte[] pdf(@RequestBody Filter filter) {
        try {
            Osm osm = new Osm(filter);
            byte[] contents = osm.getPdf();
            return contents;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value="/xml", produces="application/xml")
    public @ResponseBody Document xml(@RequestBody Filter filter) {
        try {
            Osm osm = new Osm(filter);
            return osm.getXML();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
