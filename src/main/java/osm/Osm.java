package osm;

import java.io.*;
import java.net.HttpURLConnection;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.*;
import org.apache.fop.apps.*;
import org.w3c.dom.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.*;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Osm {

    private Filter filter;
    private Document document;
    private Processor processor;

    public Osm(Filter filter) throws FetchOsmDataException, SaxonApiException, ParserConfigurationException {
        this.filter = filter;
        this.document = this.fetchOsmData(filter);



        this.processor = new Processor(false);

    }

    /**
     * fetching osm xml from overpass api
     * @param filter
     * @return parsed xml from Overpass API
     * @throws FetchOsmDataException
     */
    private Document fetchOsmData(Filter filter) throws FetchOsmDataException {
        try {
            String uri = "http://overpass-api.de/api/interpreter";
            URL url = new URL(uri);
            byte[] postData = filter.getOverpassQuery().getBytes(Charset.forName("UTF-8"));
            int postDataLength = postData.length;
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/xml");
            connection.setUseCaches(false);

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(postData);


            BufferedReader inputReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String inline = "";
            while ((inline = inputReader.readLine()) != null) {
                sb.append(inline);
            }
            return this.convertStringToXml(sb.toString());
        } catch (Exception e) {
            throw new FetchOsmDataException(e.getMessage());
        }

    }

    public Document getSvg() throws Exception {
        InputStream xslFile = Osm.class.getResourceAsStream("/xsl/osm2svg.xsl");


        Document outputDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        DOMDestination output = new DOMDestination(outputDocument);
        XsltCompiler compiler = processor.newXsltCompiler();
        compiler.setParameter(new QName("minlat"), new XdmAtomicValue(this.filter.getBottom()));
        compiler.setParameter(new QName("maxlat"), new XdmAtomicValue(this.filter.getTop()));
        compiler.setParameter(new QName("minlon"), new XdmAtomicValue(this.filter.getLeft()));
        compiler.setParameter(new QName("maxlon"), new XdmAtomicValue(this.filter.getRight()));
        XsltExecutable executable = compiler.compile(new StreamSource(xslFile));
        XsltTransformer transformer = executable.load();
        transformer.setDestination(output);
        transformer.setSource(new DOMSource(this.document));
        transformer.transform();
        return outputDocument;
    }

    public String getSvgAsString() throws Exception {
         return this.fromXmltoString(this.getSvg());
    }

    public byte[] getPdf() throws Exception {
        InputStream xslFile = Osm.class.getResourceAsStream("/xsl/svg2fo.xsl");


        Document outputDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        DOMDestination output = new DOMDestination(outputDocument);
        XsltCompiler compiler = processor.newXsltCompiler();
        XsltExecutable executable = compiler.compile(new StreamSource(xslFile));
        XsltTransformer transformer = executable.load();
        transformer.setDestination(output);

        // get svg data and set as input for the new transformation
        transformer.setSource(new DOMSource(this.getSvg()));
        transformer.transform();


        FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer1 = factory.newTransformer();

            Source src = new DOMSource(outputDocument);

            Result res = new SAXResult(fop.getDefaultHandler());

            transformer1.transform(src, res);

        } finally {
            out.close();
        }
        return out.toByteArray();
    }

    public Document getXML() {
        return this.document;
    }

    private static String fromXmltoString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    private static Document convertStringToXml(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
