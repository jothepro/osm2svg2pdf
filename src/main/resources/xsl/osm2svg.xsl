<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/2000/svg"
                version="2.0"
xmlns:osm="http://johanneskrafft.de">
    <xsl:output method="xml"
         omit-xml-declaration = "yes"
         encoding="UTF-8"
         indent="yes" />

    <xsl:param name="minlat"/>
    <xsl:param name="minlon"/>
    <xsl:param name="maxlat"/>
    <xsl:param name="maxlon"/>

    <xsl:function name="osm:waypoints">
        <xsl:param name="way"/>
        <xsl:for-each select="$way/nd">
            <xsl:variable name="ref" select="@ref"/>
            <xsl:value-of select="osm:calculatePosition(/osm/node[@id=$ref]/@lat, /osm/node[@id=$ref]/@lon)" />,</xsl:for-each>
    </xsl:function>
    <xsl:function name="osm:calculatePosition">
        <xsl:param name="lat"/>
        <xsl:param name="lon"/>
        <xsl:variable name="scaleFactor" select="1200 div ( number($maxlat) - number($minlat) )"/>
        <xsl:value-of select="(number($lon) - number($minlon))*$scaleFactor"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="1200 - (number($lat) - number($minlat))*$scaleFactor "/>


    </xsl:function>
    <xsl:template match="/">
        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1200 1200" preserveAspectRatio="xMidYMid slice">
            <style type="text/css" >
                <![CDATA[



polyline {
	stroke: #999;
	stroke-width: 1px;
	fill: none;
}

polygon.landuse {
	/*
	placeholder for not supported landuse elements
	 */
	fill: red;
}

polygon.landuse.allotments {
	fill: #EED1B0;
}

polygon.landuse.basin {
	fill: #AFD0D0;
}

polygon.landuse.brownfield {
 fill: #B0BA8F;
}

polygon.landuse.cemetery {
	fill: #9ECFAC;
}

polygon.landuse.commercial {
	fill: #F1CCCD;
}

polygon.landuse.construction {
	fill: #C3C9B2;
}

polygon.landuse.farmland {
	fill: #F7ECC9;
}

polygon.landuse.farmyard {
	fill: #E9D0A1;
}

polygon.landuse.forest {
	fill: url(#treePattern);
}

polygon.landuse.garages {
	fill: #DBE0CB;
}

polygon.landuse.grass {
	fill: #C0FAA3;
}

polygon.landuse.greenfield {
	fill: #F0EFE7;
}

polygon.landuse.greenhouse_horticulture {
	fill: #F7ECC9;
}

polygon.landuse.industrial {
	fill: #EACBE2;
}

polygon.landuse.landfill {
	fill: #B0BA8F;
}

polygon.landuse.meadow {
	fill: url(#grassPattern);
}

polygon.landuse.military {
	fill: #F8CEC8;
}

polygon.landuse.orchard {
	fill: #86E98F;
}

polygon.landuse.plant_nursery {
	fill: #4A90D9;
}

polygon.landuse.quarry {
	fill: #B6B3B4;
}

polygon.landuse.railway {
	fill: #EACBE2;
}

polygon.landuse.recreation_ground {
	fill: #C0FAA3;
}


polygon.landuse.residential {
	fill: #E0DFDF;
}

polygon.landuse.retail {
	fill: #FFC6C3;
}

polygon.landuse.village_green {
	fill: #C0FAA3;
}

polygon.landuse.vineyard {
	fill: #87EA90;
}


polygon.lake {
	fill: #B6D0D0;
}

polygon.house {
	fill: #D9D0C9;
	stroke: #C2B5AA;
	stroke-width: 1px;
}

polyline.river {
	fill: none;
	stroke: #B6D0D0;
	stroke-width: 10px;
}

polyline.track {
	fill: none;
	stroke: #A98A25;
	stroke-width: 1px;
	stroke-dasharray: 5,5;
}

polyline.service {
	fill: none;
	stroke: #FEFEFE;
	stroke-width: 3px;
}

polyline.residential {
	fill: none;
	stroke: #FEFEFE;
	stroke-width: 5px;
}

polyline.unclassified {
	fill: none;
	stroke: #FEFEFE;
	stroke-width: 5px;
}

polyline.teriary {
	fill: none;
	stroke: #FEFEFE;
	stroke-width: 7px;
}

polyline.secondary {
	fill: none;
	stroke: #F6FABB;
	stroke-width: 7px;
}

polyline.primary {
	fill: none;
	stroke: #FDD7A1;
	stroke-width: 7px;
}

polyline.trunk {
	fill: none;
	stroke: #FBB29A;
	stroke-width: 10px;
}

polyline.motorway {
	fill: none;
	stroke: #E990A0;
	stroke-width: 10px;
}

polyline.train_base {
	fill: none;
	stroke: #717171;
	stroke-width: 3px;
}

polyline.traintrack {
	fill: none;
	stroke: #FFFFFF;
	stroke-width: 2px;
	stroke-dasharray: 5,5;
}

      ]]>
            </style>

            <title>OSM2SVG</title>
            <defs>
                <pattern id="treePattern" patternUnits="userSpaceOnUse"
                         x="0" y="0" width="10" height="10"
                         viewBox="0 0 2 2"  >
                    <path d="M 0 0 L 0 2 L 2 2 L 2 0 " fill="#8BD489" />
                    <path d="M 0 1 L .5 0 L 1 1 " fill="green" />
                </pattern>
                <pattern id="grassPattern" patternUnits="userSpaceOnUse"
                         x="0" y="0" width="10" height="10"
                         viewBox="0 0 6 6" >
                    <path d="M 1 1.5 L 2 4 L 2.3 5 L 1.2 1.5"/>
                    <path d="M 1.7 1 L 2 4 L2.3 4 L2 1 " fill="green" />
                    <path d="M 2 4 L 2.1 4 L3.6 2 L 3.6 1" fill="green" />
                </pattern>
            </defs>
            <rect id="background" width="1200" height="1200" fill="#F2EFE9"/>
            <xsl:apply-templates select="/osm/way/tag[@k='landuse']"/>
            <xsl:apply-templates select="/osm/way/tag[@k='building']"/>
            <xsl:apply-templates select="/osm/way/tag[@k='highway']"/>
        </svg>
    </xsl:template>

    <xsl:template match="/osm/way/tag[@k='landuse']">
        <polygon class="landuse {@v}" points="{osm:waypoints(..)}"/>
    </xsl:template>

    <xsl:template match="/osm/way/tag[@k='building']">
        <polygon class="house" points="{osm:waypoints(..)}"/>
    </xsl:template>

    <xsl:template match="/osm/way/tag[@k='highway']">
        <polyline class="highway {@v}" points="{osm:waypoints(..)}"/>
    </xsl:template>

    <xsl:template match="/osm/way/tag[@k='railway']">
        <polyline class="train_base" points="{osm:waypoints(..)}"/>
        <polyline class="traintrack" points="{osm:waypoints(..)}"/>
    </xsl:template>





</xsl:stylesheet>
