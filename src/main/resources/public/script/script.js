var container;
var current_lat = 48.84106;
var current_lon = 10.06773;
var current_zoom = 0.003;
var show_houses = true;
var show_nature = true;
var show_streets = true;
var loading_screen;

function update() {
    loading_screen.style.display = "block";
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            loading_screen.style.display = "none";
            if (httpRequest.status === 200) {
                container.style.left = '-100vw';
                container.style.top = '-100vh';
                container.style.transition = "transform 0s ease-in-out";
                container.style.transform = "scale(1)";
                container.innerHTML = httpRequest.responseText;
            } else {
                alert('There was a problem with the request.');
            }
        }
    };
    httpRequest.open('POST', "/api/svg");
    var data = {
        lat: current_lat,
        lon: current_lon,
        zoom: current_zoom,
        streets: show_streets,
        buildings: show_houses,
        nature: show_nature
    };
    httpRequest.setRequestHeader("Content-Type", "application/json");
    httpRequest.send(JSON.stringify(data));
}

function pdf() {
    loading_screen.style.display = "block";
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', "/api/pdf");
    var data = {
        lat: current_lat,
        lon: current_lon,
        zoom: current_zoom,
        streets: show_streets,
        buildings: show_houses,
        nature: show_nature
    };
    httpRequest.responseType = "arraybuffer";
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            loading_screen.style.display = "none";
            if (httpRequest.status === 200) {
                var filename = Math.floor(Date.now() / 1000) + "_map.pdf";
                var type = httpRequest.getResponseHeader('Content-Type');

                var blob = new Blob([this.response], { type: type });
                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }

                    setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
            } else {
                alert('There was a problem with the request.');
            }
        }
    };
    httpRequest.setRequestHeader("Content-Type", "application/json");
    httpRequest.send(JSON.stringify(data));
}

window.onload = function () {

    container = document.getElementById("osm2svg_output");

    document.getElementById("zoom").addEventListener("change", function (event) {
        container.style.transition = "transform .2s ease-in-out";
        var animation_zoom = ( 1 - ( (parseFloat(this.value) - current_zoom ) / parseFloat(this.value)   ) );
        container.style.transform = "scale("+(animation_zoom )+")";
        current_zoom = this.value;
        update();

    });

    document.getElementById("lat").addEventListener("change", function () {
        current_lat = this.value;
        update();

    });

    document.getElementById("lon").addEventListener("change", function () {
        current_lon = this.value;
        update();

    });

    document.getElementById("show_houses").addEventListener("change", function() {
        show_houses = this.checked;
        update()
    });

    document.getElementById("show_nature").addEventListener("change", function() {
        show_nature = this.checked;
        update()
    });

    document.getElementById("show_streets").addEventListener("change", function() {
        show_streets = this.checked;
        update()
    });

    container.addEventListener("mousedown", function (event) {
        var initialXOffset = container.offsetLeft - event.pageX;
        var initialYOffset = container.offsetTop - event.pageY;
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var drawingArea = Math.min(w, h);

        var mouseMoveHandler = function (event) {
            container.style.left = ( event.pageX + initialXOffset ) + "px";
            container.style.top = ( event.pageY + initialYOffset ) + "px";
        };

        var mouseUpHandler = function (event) {
            container.removeEventListener("mousemove", mouseMoveHandler);
            container.removeEventListener("mouseup", mouseUpHandler);

            mapOffsetX = ( ( (event.pageX  + initialXOffset + w) * ( 1200 / drawingArea ) ) / 1200 ) * current_zoom;
            mapOffsetY = ( ( (event.pageY  + initialYOffset + h) * ( 1200 / drawingArea ) ) / 1200 ) * current_zoom;

            if(mapOffsetY != 0 || mapOffsetY != 0 ) {
                current_lat = current_lat + ( mapOffsetY);
                current_lon = current_lon - ( mapOffsetX );
                update();
            }

        };

        container.addEventListener("mousemove", mouseMoveHandler);
        container.addEventListener("mouseup", mouseUpHandler);

    });

    document.getElementById("lat").value = current_lat;
    document.getElementById("lon").value = current_lon;
    document.getElementById("zoom").value = current_zoom;
    document.getElementById("show_streets").checked = show_streets;
    document.getElementById("show_houses").checked = show_houses;
    document.getElementById("show_nature").checked = show_nature;
    loading_screen = document.getElementById("loading_screen");
    update();
};


